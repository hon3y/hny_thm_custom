page {
    shortcutIcon = {$plugin.tx_hive_thm_custom.settings.production.includePath.public}Assets/Icons/favicon.ico
}

[globalVar = LIT:1 > {$plugin.tx_hive_cfg_typoscript.settings.gulp}]
    # not in use (BS4)
    #page {
    #    includeCSS {
    #      hive_thm_custom_theme = {$plugin.tx_hive_thm_custom.settings.production.includePath.private}Assets/Less/includeCSS/post/customTheme.less
    #      hive_thm_custom_theme.media = all
    #    }
    #}
[else]

     <INCLUDE_TYPOSCRIPT: source="FILE:EXT:hive_thm_custom/Resources/Public/Assets/Gulp/Js/headerData/headerData.ts">
     <INCLUDE_TYPOSCRIPT: source="FILE:EXT:hive_thm_custom/Resources/Public/Assets/Gulp/Js/footerData/footerData.ts">

    page {
            includeCSS {
              hive_thm_custom_includeCSS = {$plugin.tx_hive_thm_custom.settings.production.includePath.public}Assets/Gulp/Scss/includeCSS/includeCSS.min.css
              hive_thm_custom_theme.media = all
            }

            includeJSLibs {
                hive_thm_custom_includeJSLibs = {$plugin.tx_hive_thm_custom.settings.production.includePath.public}Assets/Gulp/Js/includeJSLibs/includeJSLibs.min.js
            }

            includeJSFooterlibs {
                hive_thm_custom_includeJSFooterlibs = {$plugin.tx_hive_thm_custom.settings.production.includePath.public}Assets/Gulp/Js/includeJSFooterlibs/includeJSFooterlibs.min.js
            }
        }
[global]

[globalVar = LIT:1 = {$plugin.tx_hive_thm_webfontloader.settings.production.optional.active}] && [globalVar = LIT:1 > {$plugin.tx_hive_cfg_typoscript.settings.gulp}]
page {
    headerData.3330141 = TEXT
    headerData.3330141.value (
<script type="text/javascript">;(function(){var a=setInterval(function(){if(typeof WebFont=="undefined"){}else{clearInterval(a);if(hive_cfg_typoscript_sStage=="prototype"||hive_cfg_typoscript_sStage=="development"){console.info("use_webfontloader initialize")}hive_thm_webfontloader__webFontConfig={google:{families:["Ubuntu:300,400,700"]}};WebFont.load(hive_thm_webfontloader__webFontConfig)}},500)})();</script>
    )


}
[global]

#plugin.tx_hive_thm_bs_toolkit.settings.lib.bsToolkit.bUseBsToolkit = {$plugin.tx_hive_thm_bs_toolkit.settings.lib.bsToolkit.bUseBsToolkit}


#########################
## example config lang ##
########################
#[PIDinRootline = 6]
#    config.sys_language_uid = 1
#    config.language = de
#    config.locale_all = de_DE.utf8
#    config.htmlTag_langKey = de
#[global]
